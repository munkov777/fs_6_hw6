import Enums.Species;

public class RoboCat extends Pet implements Foul{
    Species species =  Species.ROBOCAT;

    public RoboCat(String nickname, int age, int trickLevel, String[] habits, Species species) {
        super(nickname, age, trickLevel, habits);
        this.species = species;
    }

    public RoboCat(String nickname, Species species) {
        super(nickname);
        this.species = species;
    }

    public RoboCat(Species species) {
        this.species = species;
    }

    @Override
    public void foul(){
            System.out.println("Нужно хорошо замести следы...");
        }



    @Override
    public void respond() {
    }

}
