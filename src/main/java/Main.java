import Enums.DayOfWeek;
import Enums.Species;


public class Main {
    public static void main(String[] args) {
        //first family
        Man viktor = new Man("Viktor", "Munko", 40);
        viktor.setIq(150);
        String[][] scheduleViktor = new String[2][2];
        scheduleViktor[0][0] = DayOfWeek.SATURDAY.name();
        scheduleViktor[0][1] = "Play football";
        scheduleViktor[1][0] = DayOfWeek.SUNDAY.name();
        scheduleViktor[1][1] = "Goto the gym";
        viktor.setSchedule(scheduleViktor);
        Woman halyna = new Woman("Halyna", "Munko", 38);
        halyna.setIq(150);
        String[][] scheduleHalyna = new String[2][2];
        scheduleHalyna[0][0] = DayOfWeek.SATURDAY.name();
        scheduleHalyna[0][1] = "Go shopping";
        scheduleHalyna[1][0] = DayOfWeek.SUNDAY.name();
        scheduleHalyna[1][1] = "Play tennis";
        halyna.setSchedule(scheduleHalyna);
        Family familyWick = new Family(halyna, viktor);
        Dog rej = new Dog("Rej", Species.DOG);
        familyWick.setPet(rej);
        Man tymur = new Man("Tymur", "Munko", 1990, 97, scheduleViktor);
        familyWick.addChild(tymur);
        tymur.setFamily(familyWick);
        Man bohdan = new Man("Bohdan", "Munko", 2008, 109, scheduleViktor);
        familyWick.addChild(bohdan);
        bohdan.setFamily(familyWick);

        System.out.println(familyWick);
    }
}
