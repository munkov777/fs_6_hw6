import Enums.Species;

public class Fish extends Pet{
Species species = Species.FISH;

    public Fish(String nickname, int age, int trickLevel, String[] habits, Species species) {
        super(nickname, age, trickLevel, habits);
        this.species = species;
    }

    public Fish(String nickname, Species species) {
        super(nickname);
        this.species = species;
    }

    public Fish(Species species) {
        this.species = species;
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + "." + " Я соскучился!");
    }
}
