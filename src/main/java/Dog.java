import Enums.Species;

public class Dog extends Pet implements Foul{

    Species species = Species.DOG;


    //public Dog(Species nickname, int age, int trickLevel, String[] habits) {
       // super(nickname, age, trickLevel, habits);
   // }


    public Dog(String nickname, int age, int trickLevel, String[] habits, Species species) {
        super(nickname, age, trickLevel, habits);
        this.species = species;
    }

    public Dog(String nickname, Species species) {
        super(nickname);
        this.species = species;
    }

    public Dog(Species species) {
        this.species = species;
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + super.getNickname() + "." + " Я соскучился!");
    }

   @Override
   public void foul() {
       System.out.println("Потрібно добре замести сліди...");

   }
}


