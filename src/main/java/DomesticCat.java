import Enums.Species;

public class DomesticCat extends Pet implements Foul{
    Species species = Species.CAT;

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + "." + " Я соскучился!");

    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");

    }

}
