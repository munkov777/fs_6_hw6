import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {
    Human mather = new Human("Halyna", "Munko", 38);
    Human father = new Human("Viktor", "Munko", 40);

    @Test
    void getName() {
        String expected = mather.getName();
        String actual = "Halyna";
        Assert.assertEquals(expected,actual);
    }

    @Test
    void getSurname() {
        String expected = mather.getSurname();
        String actual = "Munko";
        Assert.assertEquals(expected,actual);
    }

    @Test
    void getYear() {
        int expected = mather.getYear();
        int actual = 38;
        Assert.assertEquals(expected, actual);
    }
    @Test
    void Hashcode(){
        assertTrue(mather.hashCode()!=father.hashCode());
    }


}